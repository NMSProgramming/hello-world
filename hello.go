package main

// Hello world program.

// Choose a line and replace "[type your name here]" with your name, then
// commit that to gitlab, then pull in all the changes and see who else
// added their name.

import (
	"fmt"
)

// Main function. Prints our names.
func main() {
	fmt.Println("Hello world!")
	fmt.Println("1. I am groot -Groot")
	fmt.Println("2.  hi abby im watching youu#@$$%]")
	fmt.Println("3. [Audrey]")
	fmt.Println("4. Taya")
	fmt.Println("5. [type your name here]")
	fmt.Println("6. [type your name here]")
}
